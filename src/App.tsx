import './App.css';
import React from 'react';
import Puzzle from './components/Puzzle/Puzzle';
import puzzles from './puzzles';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

const App: React.FC = () => {
  return (
    <Router>
      <Routes>
        <Route index element={<PuzzlesList />} />
        <Route path=":puzzleSlug" element={<Puzzle />} />
      </Routes>
    </Router>
  );
};

const PuzzlesList: React.FC = () => (
  <nav>
    <ul>
      {puzzles.map(({ slug, name }) => (
        <Link to={`/${slug}`} key={slug}>
          <li className="box-shadow">{name}</li>
        </Link>
      ))}
    </ul>
  </nav>
);

export default App;
