import { Mode, Direction, NodeType, Tile } from './constants';
import { Command } from './hooks/useComputer/commands';

export interface Egress {
  direction: Direction;
  data?: number;
}

export interface Ingress {
  direction: Direction;
}

// Called when a node puts a value out for another to receive
// NOT called when value is taken by other node.
export type OnEgress = (
  nodeType: NodeType,
  nodeId: string,
  valueSent?: number
) => void;

// Called when a node receives a value.
export type OnIngress = (
  nodeType: NodeType,
  nodeId: string,
  valueReceived: number
) => void;

export interface InitialNodeState {
  row?: number;
  column?: number;
  linesOfCode?: Array<string>;
  curLineIndex?: number;
  isDisabled?: boolean;
  hasError?: boolean;
  acc?: number;
  bak?: number;
  last?: string;
  mode?: Mode;

  egress?: Egress;
  ingress?: Ingress;
}

export interface TestNodeConfig {
  id: string;
  column: number;
}

export interface TestInputValues {
  id: string;
  valuesToSend: Array<number>;
}

export interface TestOutputValues {
  id: string;
  expectedValues: Array<number>;
}

export interface TestValues {
  inputs: Array<TestInputValues>;
  outputs: Array<TestOutputValues>;
}

export interface ComputeNode {
  id: string;
  type: NodeType;
  row: number;
  column: number;
  cursorIndex: number;
  linesOfCode: Array<string>;
  curLineIndex: number;
  isDisabled: boolean;
  hasError: boolean;
  acc: number;
  bak: number;
  last?: string;
  mode: Mode;

  hasUp?: boolean;
  hasRight?: boolean;
  hasDown?: boolean;
  hasLeft?: boolean;

  egress?: Egress;
  ingress?: Ingress;
}

export type CommandPrepareFn = (
  node: ComputeNode,
  lineOfCode: string
) => ComputeNode;

export type CommandExecuteFnArgs = {
  node: ComputeNode;
  lineOfCode: string;
  adjacentNodes: AdjacentNodesType;
  onEgress: OnEgress;
  onIngress: OnIngress;
};
export type CommandExecuteFn = ({
  node,
  lineOfCode,
  adjacentNodes,
  onEgress,
  onIngress,
}: CommandExecuteFnArgs) => ComputeNode;

export interface CommandType {
  type: Command;
  matcher?: RegExp;
  prepare: CommandPrepareFn;
  execute: CommandExecuteFn;
}

export interface AdjacentNodesType {
  upNode?: ComputeNode;
  rightNode?: ComputeNode;
  downNode?: ComputeNode;
  leftNode?: ComputeNode;
}

export interface ComputerState {
  nodes: Array<ComputeNode>;
  isRunning: boolean;
}

export type SetNodeLinesOfCodeFn = (
  row: number,
  column: number,
  linesOfCode: Array<string>,
  cursorIndex: number
) => void;

export type SetInputValuesToSendFn = (
  nodeId: string,
  valuesToSend: Array<number>
) => void;

export type SetInputValuesFn = (inputs: Array<TestInputValues>) => void;
export type GetTestValuesFn = (getRandomInt: RandomIntGenerator) => TestValues;

export type StartFn = () => void;
export type StartFastFn = () => void;
export type StepFn = () => void;
export type ComputerStepFn = (onEgress: OnEgress, onIngress: OnIngress) => void;
export type StopFn = () => void;

export interface ComputerType extends ComputerState {
  setNodeLinesOfCode: SetNodeLinesOfCodeFn;
  setInputValues: SetInputValuesFn;
  start: StartFn;
  step: ComputerStepFn;
  stop: StopFn;
}

export interface TestInputType {
  id: string;
  valuesToSend: Array<number>;
  curValueToSendIndex: number;
}

export interface TestOutputType {
  id: string;
  expectedValues: Array<number>;
  valuesReceived: Array<number>;
}

export type RandomIntGenerator = (
  inclusiveMin: number,
  inclusiveMax: number
) => number;

export interface PuzzleType {
  computer: ComputerType;
  testIndex: number;
  inputs: Array<TestInputType>;
  outputs: Array<TestOutputType>;
  start: StartFn;
  startFast: StartFastFn;
  step: StepFn;
  stop: StopFn;
}

export interface PuzzleConfiguration {
  slug: string;
  name: string;
  description: Array<string>;
  creator: string;
  layout: Array<Tile>;
  inputNodes: Array<TestNodeConfig>;
  outputNodes: Array<TestNodeConfig>;
  getTestValues: GetTestValuesFn;
}
