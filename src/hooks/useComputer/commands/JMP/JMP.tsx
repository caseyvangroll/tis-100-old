import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { scanToNextCommand } from '../helpers';

export function executeJmp({
  node,
  lineOfCode,
}: CommandExecuteFnArgs): ComputeNode {
  const [, label] = lineOfCode.split(/\s/g);

  const nextCurLineIndex =
    node.linesOfCode.findIndex(line => line.trim().startsWith(`${label}:`)) ||
    -1;

  return scanToNextCommand({
    ...node,
    curLineIndex: nextCurLineIndex,
  });
}
