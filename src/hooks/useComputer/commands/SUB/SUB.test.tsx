import { executeSub } from './SUB';
import { nodeDefaults } from '../../nodes';

describe('executeSub', () => {
  const onIngress = jest.fn();
  const onEgress = jest.fn();
  afterEach(() => {
    onIngress.mockClear();
    onEgress.mockClear();
  });

  test('can subtract a constant number from acc', () => {
    const node = {
      ...nodeDefaults,
      id: 'subNode',
      row: 0,
      column: 0,
      linesOfCode: ['SUB 3'],
      curLineIndex: 0,
      acc: 1,
    };
    const lineOfCode = node.linesOfCode[node.curLineIndex];
    const adjacentNodes = {};

    const expected = {
      ...node,
      acc: -2,
    };

    const result = executeSub({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    });
    expect(result).toEqual(expected);
  });

  test('can add data from bus to acc', () => {
    // TODO
  });
});
