import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { Direction } from '../../../../constants';
import { resolveOperand, scanToNextCommand, isDirection } from '../helpers';

export function prepareSub(node: ComputeNode, lineOfCode: string): ComputeNode {
  const [, subtractor] = lineOfCode.split(/\s/g);

  // Switch to waiting for data if needed
  if (!node.egress && isDirection(subtractor)) {
    return {
      ...node,
      ingress: {
        direction: subtractor as Direction,
      },
    };
  }

  return node;
}

export function executeSub({
  node,
  lineOfCode,
  adjacentNodes,
  onIngress,
}: CommandExecuteFnArgs): ComputeNode {
  const [, subtractor] = lineOfCode.split(/\s/g);

  // If constant or other node exposing egress then pick it up and sub from acc
  const resolvedSubtractor = resolveOperand(node, subtractor, adjacentNodes);
  if (resolvedSubtractor !== undefined) {
    if (isDirection(subtractor)) {
      onIngress(node.type, node.id, resolvedSubtractor);
    }
    return scanToNextCommand({
      ...node,
      ingress: undefined,
      acc: node.acc - resolvedSubtractor,
    });
  }

  return node;
}
