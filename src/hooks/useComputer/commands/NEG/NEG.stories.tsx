import { Meta } from '@storybook/react';
import { Sandbox } from '../../../../components/Computer/Computer.stories';

export default {
  title: 'Commands/NEG',
} as Meta;

export const WithZeroAcc = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: 0, linesOfCode: ['NEG'] }]} />
);

export const WithPositiveACC = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: 3, linesOfCode: ['NEG'] }]} />
);

export const WithNegativeACC = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: -3, linesOfCode: ['NEG'] }]} />
);
