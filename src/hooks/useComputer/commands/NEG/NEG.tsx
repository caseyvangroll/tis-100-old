import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { scanToNextCommand } from '../helpers';

export function executeNeg({ node }: CommandExecuteFnArgs): ComputeNode {
  return scanToNextCommand({
    ...node,
    acc: -node.acc,
  });
}
