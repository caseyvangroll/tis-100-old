import { executeNeg } from './NEG';
import { nodeDefaults } from '../../nodes';

describe('executeNeg', () => {
  const onIngress = jest.fn();
  const onEgress = jest.fn();
  afterEach(() => {
    onIngress.mockClear();
    onEgress.mockClear();
  });

  test('negates positive number', () => {
    const node = {
      ...nodeDefaults,
      id: 'negNode',
      row: 0,
      column: 0,
      linesOfCode: ['NEG'],
      curLineIndex: 0,
      acc: 2,
    };
    const lineOfCode = node.linesOfCode[node.curLineIndex];
    const adjacentNodes = {};

    const expected = {
      ...node,
      acc: -2,
    };

    const result = executeNeg({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    });
    expect(result).toEqual(expected);
  });

  test('negates negative number', () => {
    const node = {
      ...nodeDefaults,
      id: 'negNode',
      row: 0,
      column: 0,
      linesOfCode: ['NEG'],
      curLineIndex: 0,
      acc: -2,
    };
    const lineOfCode = node.linesOfCode[node.curLineIndex];
    const adjacentNodes = {};

    const expected = {
      ...node,
      acc: 2,
    };

    const result = executeNeg({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    });
    expect(result).toEqual(expected);
  });
});
