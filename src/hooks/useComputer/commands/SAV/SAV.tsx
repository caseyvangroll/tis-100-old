import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { scanToNextCommand } from '../helpers';

export function executeSav({ node }: CommandExecuteFnArgs): ComputeNode {
  return scanToNextCommand({
    ...node,
    bak: node.acc,
  });
}
