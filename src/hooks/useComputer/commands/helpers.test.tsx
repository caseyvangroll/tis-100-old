import { scanToNextCommand } from './helpers';
import { nodeDefaults } from '../nodes';

describe('scanToNextCommand', () => {
  describe('forward', () => {
    test('should result in no change if no lines of code', () => {
      const node = {
        ...nodeDefaults,
        id: 'jezNode',
        row: 0,
        column: 0,
        linesOfCode: [],
        curLineIndex: -1,
      };
      const expected = {
        ...node,
      };

      expect(scanToNextCommand(node)).toEqual(expected);
    });
  });
  // describe('reverse', () => {
  //   test('should result in no change if no lines of code', () => {
  //     const node = {

  //     }
  //     const result = scanToNextCommand(node, );
  //   })
  // })
});
