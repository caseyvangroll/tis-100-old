import { Meta } from '@storybook/react';
import { Sandbox } from '../../../../components/Computer/Computer.stories';

export default {
  title: 'Commands/ADD',
} as Meta;

export const Constant = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ linesOfCode: ['ADD 3'] }]} />
);

export const FromACC = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: 3, linesOfCode: ['ADD ACC'] }]} />
);

export const FromAdjacentNode = (): JSX.Element => (
  <Sandbox
    initialNodeStates={[
      { linesOfCode: ['MOV 3, RIGHT'] },
      { linesOfCode: ['ADD LEFT'] },
    ]}
  />
);
