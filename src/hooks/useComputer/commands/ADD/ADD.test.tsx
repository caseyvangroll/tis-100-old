import { executeAdd } from './ADD';
import { Direction } from '../../../../constants';
import { nodeDefaults } from '../../nodes';

describe('executeAdd', () => {
  const onIngress = jest.fn();
  const onEgress = jest.fn();
  afterEach(() => {
    onIngress.mockClear();
    onEgress.mockClear();
  });

  test('can add a constant number to acc', () => {
    const node = {
      ...nodeDefaults,
      id: 'addNode',
      row: 0,
      column: 0,
      linesOfCode: ['ADD 3'],
      curLineIndex: 0,
      acc: 2,
    };
    const lineOfCode = node.linesOfCode[node.curLineIndex];
    const adjacentNodes = {};

    const expected = {
      ...node,
      acc: 5,
    };

    const result = executeAdd({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    });
    expect(result).toEqual(expected);
  });

  test('can add data from adjacent node to acc', () => {
    const node = {
      ...nodeDefaults,
      id: 'addNode',
      row: 1,
      column: 0,
      linesOfCode: ['ADD UP'],
      curLineIndex: 0,
      acc: 2,
      hasUp: true,
    };
    const lineOfCode = node.linesOfCode[node.curLineIndex];
    const adjacentNodes = {
      upNode: {
        ...nodeDefaults,
        id: 'upNode',
        row: 0,
        column: 0,
        linesOfCode: ['MOV 3, DOWN'],
        curLineIndex: 0,
        hasDown: true,
        egress: {
          direction: Direction.Down,
          data: 3,
        },
      },
    };

    const expected = {
      ...node,
      acc: 5,
    };

    const result = executeAdd({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    });
    expect(result).toEqual(expected);
  });
});
