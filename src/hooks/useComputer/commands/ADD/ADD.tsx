import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { Direction } from '../../../../constants';
import { resolveOperand, scanToNextCommand, isDirection } from '../helpers';

export function prepareAdd(node: ComputeNode, lineOfCode: string): ComputeNode {
  const [, addend] = lineOfCode.split(/\s/g);

  // Switch to waiting for data if needed
  if (!node.egress && isDirection(addend)) {
    return {
      ...node,
      ingress: {
        direction: addend as Direction,
      },
    };
  }

  return node;
}

export function executeAdd({
  node,
  lineOfCode,
  adjacentNodes,
  onIngress,
}: CommandExecuteFnArgs): ComputeNode {
  const [, addend] = lineOfCode.split(/\s/g);

  // If constant or other node exposing egress then pick it up and add to acc
  const resolvedAddend = resolveOperand(node, addend, adjacentNodes);
  if (resolvedAddend !== undefined) {
    if (isDirection(addend)) {
      onIngress(node.type, node.id, resolvedAddend);
    }

    return scanToNextCommand({
      ...node,
      ingress: undefined,
      acc: node.acc + resolvedAddend,
    });
  }

  return node;
}
