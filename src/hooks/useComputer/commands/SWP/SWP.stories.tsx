import { Meta } from '@storybook/react';
import { Sandbox } from '../../../../components/Computer/Computer.stories';

export default {
  title: 'Commands/SWP',
} as Meta;

export const WithZeroBAK = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: 4, bak: 0, linesOfCode: ['SWP'] }]} />
);

export const WithZeroACC = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: 0, bak: -4, linesOfCode: ['SWP'] }]} />
);

export const WithNonzeroBoth = (): JSX.Element => (
  <Sandbox initialNodeStates={[{ acc: 3, bak: -4, linesOfCode: ['SWP'] }]} />
);
