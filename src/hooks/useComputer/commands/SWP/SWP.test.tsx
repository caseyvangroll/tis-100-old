import { executeSwp } from './SWP';
import { nodeDefaults } from '../../nodes';

describe('executeSwp', () => {
  const onIngress = jest.fn();
  const onEgress = jest.fn();
  afterEach(() => {
    onIngress.mockClear();
    onEgress.mockClear();
  });

  test('swaps acc and bak', () => {
    const node = {
      ...nodeDefaults,
      id: 'swpNode',
      row: 0,
      column: 0,
      linesOfCode: ['SWP'],
      curLineIndex: 0,
      acc: 4,
      bak: -19,
    };
    const lineOfCode = node.linesOfCode[node.curLineIndex];
    const adjacentNodes = {};

    const expected = {
      ...node,
      acc: -19,
      bak: 4,
    };

    const result = executeSwp({
      node,
      lineOfCode,
      adjacentNodes,
      onEgress,
      onIngress,
    });
    expect(result).toEqual(expected);
  });
});
