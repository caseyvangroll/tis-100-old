import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { Direction } from '../../../../constants';
import { resolveOperand, scanToNextCommand, isDirection } from '../helpers';

// TODO - all these fns need to handle ANY/LAST

export function prepareMov(node: ComputeNode, lineOfCode: string): ComputeNode {
  const [, source] = lineOfCode.replace(/,/g, '').split(/\s/g);

  if (!node.egress && isDirection(source)) {
    return {
      ...node,
      ingress: {
        direction: source as Direction,
      },
    };
  }

  return node;
}

export function executeMov({
  node,
  lineOfCode,
  adjacentNodes,
  onEgress,
  onIngress,
}: CommandExecuteFnArgs): ComputeNode {
  const [, source, destination] = lineOfCode.replace(/,/g, '').split(/\s/g);
  // If other node ready to pick up egress then clear it and advance to next line
  if (
    node.egress &&
    ((destination === Direction.Up &&
      adjacentNodes.upNode?.ingress?.direction === Direction.Down) ||
      (destination === Direction.Right &&
        adjacentNodes.rightNode?.ingress?.direction === Direction.Left) ||
      (destination === Direction.Down &&
        adjacentNodes.downNode?.ingress?.direction === Direction.Up) ||
      (destination === Direction.Left &&
        adjacentNodes.leftNode?.ingress?.direction === Direction.Right))
  ) {
    onEgress(node.type, node.id, node.egress.data);
    return scanToNextCommand({
      ...node,
      egress: undefined,
    });
  }
  // If have egress and not ready to leave then don't take anything else in
  else if (node.egress) {
    return node;
  }

  // If constant or other node exposing egress then pick it up and put where needed
  const resolvedSource = resolveOperand(node, source, adjacentNodes);
  if (resolvedSource !== undefined) {
    if (node.ingress) {
      onIngress(node.type, node.id, resolvedSource);
    }

    if (destination === 'ACC') {
      return scanToNextCommand({
        ...node,
        ingress: undefined,
        acc: resolvedSource,
      });
    }

    if (isDirection(destination)) {
      return {
        ...node,
        ingress: undefined,
        egress: {
          direction: destination as Direction,
          data: resolvedSource,
        },
      };
    }
  }

  return node;
}
