import { CommandType, ComputeNode, OnEgress, OnIngress } from '../../../types';
import { prepareMov, executeMov } from './MOV/MOV';
import { executeSwp } from './SWP/SWP';
import { executeSav } from './SAV/SAV';
import { prepareAdd, executeAdd } from './ADD/ADD';
import { executeSub } from './SUB/SUB';
import { executeNeg } from './NEG/NEG';
import { prepareNop, executeNop } from './NOP/NOP';
import { executeJmp } from './JMP/JMP';
import { executeJez } from './JEZ/JEZ';
import { executeJnz } from './JNZ/JNZ';
import { executeJgz } from './JGZ/JGZ';
import { executeJlz } from './JLZ/JLZ';
import { executeJro } from './JRO/JRO';

export enum Command {
  NOP = 'NOP',
  MOV = 'MOV',
  SWP = 'SWP',
  SAV = 'SAV',
  ADD = 'ADD',
  SUB = 'SUB',
  NEG = 'NEG',
  JMP = 'JMP',
  JEZ = 'JEZ',
  JNZ = 'JNZ',
  JGZ = 'JGZ',
  JLZ = 'JLZ',
  JRO = 'JRO',
}

export const commands = [
  {
    type: Command.MOV,
    matcher:
      /^[\s,]*MOV[\s,]+(-?\d+|ACC|UP|RIGHT|DOWN|LEFT|LAST|ANY)[\s,]+(ACC|UP|RIGHT|DOWN|LEFT|LAST|ANY)[\s,]*(#.*)?$/i,
    prepare: prepareMov,
    execute: executeMov,
  },
  {
    type: Command.SWP,
    matcher: /^[\s,]*SWP[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeSwp,
  },
  {
    type: Command.SAV,
    matcher: /^[\s,]*SAV[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeSav,
  },
  {
    type: Command.ADD,
    matcher:
      /^[\s,]*ADD[\s,]+(-?\d+|ACC|UP|RIGHT|DOWN|LEFT|LAST|ANY)[\s,]*(#.*)?$/i,
    prepare: prepareAdd,
    execute: executeAdd,
  },
  {
    type: Command.SUB,
    matcher:
      /^[\s,]*SUB[\s,]+(-?\d+|ACC|UP|RIGHT|DOWN|LEFT|LAST|ANY)[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeSub,
  },
  {
    type: Command.NEG,
    matcher: /^[\s,]*NEG[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeNeg,
  },
  {
    type: Command.JMP,
    matcher: /^[\s,]*JMP[\s,]+([^#\s,]+)[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeJmp,
  },
  {
    type: Command.JEZ,
    matcher: /^[\s,]*JEZ[\s,]+([^#\s,]+)[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeJez,
  },
  {
    type: Command.JNZ,
    matcher: /^[\s,]*JNZ[\s,]+([^#\s,]+)[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeJnz,
  },
  {
    type: Command.JGZ,
    matcher: /^[\s,]*JGZ[\s,]+([^#\s,]+)[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeJgz,
  },
  {
    type: Command.JLZ,
    matcher: /^[\s,]*JLZ[\s,]+([^#\s,]+)[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeJlz,
  },
  {
    type: Command.JRO,
    matcher: /^[\s,]*JRO[\s,]+-?[\d]+[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeJro,
  },
  {
    type: Command.NOP,
    matcher: /^[\s,]*NOP[\s,]*(#.*)?$/i,
    prepare: prepareNop,
    execute: executeNop,
  },
];

export const findCommand = (lineOfCode?: string): CommandType => {
  return (
    commands.find(({ matcher }) => matcher.test(lineOfCode || '')) || {
      type: Command.NOP,
      prepare: prepareNop,
      execute: executeNop,
    }
  );
};

export const prepareCommand = (node: ComputeNode): ComputeNode => {
  if (node.isDisabled) {
    return node;
  }

  const lineOfCode = node.linesOfCode[node.curLineIndex];
  const command = findCommand(lineOfCode);
  return command.prepare(node, lineOfCode);
};

export const executeCommand = (
  node: ComputeNode,
  preparedNodes: Array<ComputeNode>,
  onEgress: OnEgress,
  onIngress: OnIngress
): ComputeNode => {
  if (node.isDisabled) {
    return node;
  }

  const adjacentNodes = {
    upNode: preparedNodes.find(
      ({ row, column }) => row === node.row - 1 && column === node.column
    ),
    rightNode: preparedNodes.find(
      ({ row, column }) => row === node.row && column === node.column + 1
    ),
    downNode: preparedNodes.find(
      ({ row, column }) => row === node.row + 1 && column === node.column
    ),
    leftNode: preparedNodes.find(
      ({ row, column }) => row === node.row && column === node.column - 1
    ),
  };

  const lineOfCode = node.linesOfCode[node.curLineIndex];
  const command = findCommand(lineOfCode);
  return command.execute({
    node,
    lineOfCode,
    adjacentNodes,
    onEgress,
    onIngress,
  });
};

export default commands;
