import { ComputeNode, CommandExecuteFnArgs } from '../../../../types';
import { executeJmp } from '../JMP/JMP';
import { scanToNextCommand } from '../helpers';

export function executeJez(args: CommandExecuteFnArgs): ComputeNode {
  if (args.node.acc === 0) {
    return executeJmp(args);
  }

  return scanToNextCommand(args.node);
}
