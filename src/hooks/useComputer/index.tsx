import { useState, useMemo, useCallback } from 'react';
import { Tile, LAYOUT_WIDTH } from '../../constants';
import {
  InitialNodeState,
  ComputerState,
  ComputerType,
  TestInputValues,
  TestNodeConfig,
  ComputeNode,
  OnEgress,
  OnIngress,
} from '../../types';
import {
  flagAdjacentNodes,
  getInputNodeLinesOfCode,
  resetNode,
  resolveInputNodeDefaults,
  resolveOutputNodeDefaults,
  resolveNodeDefaults,
  updateNodeLinesOfCode,
} from './nodes';
import { prepareCommand, executeCommand } from './commands';

export const getDefaultState = (
  layout: Array<Tile>,
  inputNodes: Array<TestNodeConfig>,
  outputNodes: Array<TestNodeConfig>,
  initialNodeStates?: Array<InitialNodeState>
): ComputerState => {
  const nodes = [
    ...resolveNodeDefaults(layout, initialNodeStates),
    ...resolveInputNodeDefaults(inputNodes),
    ...resolveOutputNodeDefaults(outputNodes),
  ];
  return {
    nodes: flagAdjacentNodes(nodes),
    isRunning: false,
  };
};

export default function useComputer(
  layout: Array<Tile>,
  inputNodes: Array<TestNodeConfig>,
  outputNodes: Array<TestNodeConfig>,
  initialNodeStates?: Array<InitialNodeState>
): ComputerType {
  const defaultState = useMemo(
    () => getDefaultState(layout, inputNodes, outputNodes, initialNodeStates),
    [layout, inputNodes, outputNodes, initialNodeStates]
  );

  const [nodes, setNodes] = useState(defaultState.nodes);
  const [isRunning, setIsRunning] = useState(defaultState.isRunning);

  const setNodeLinesOfCode = useCallback(
    (
      row: number,
      column: number,
      linesOfCode: Array<string>,
      cursorIndex: number
    ) => {
      const nodeToUpdateIndex = row * LAYOUT_WIDTH + column;
      setNodes(prevNodes =>
        prevNodes.map((node, i) =>
          i === nodeToUpdateIndex
            ? updateNodeLinesOfCode(node, linesOfCode, cursorIndex)
            : node
        )
      );
    },
    [setNodes]
  );

  const step = (onEgress: OnEgress, onIngress: OnIngress) => {
    const preparedNodes = nodes.map(prepareCommand);
    const nextNodes = preparedNodes.map((node: ComputeNode) =>
      executeCommand(node, preparedNodes, onEgress, onIngress)
    );
    setNodes(nextNodes);
    setIsRunning(true);
  };

  function start() {
    setIsRunning(true);
  }

  const stop = useCallback(() => {
    setNodes(prevNodes => prevNodes.map(resetNode));
    setIsRunning(false);
  }, [setNodes, setIsRunning]);

  const setInputValues = useCallback(
    (inputs: Array<TestInputValues>) => {
      setNodes(prevNodes =>
        prevNodes.map(node => {
          const matchingInput = inputs.find(input => input.id === node.id);
          if (matchingInput) {
            return {
              ...node,
              curLineIndex: -1,
              linesOfCode: getInputNodeLinesOfCode(matchingInput.valuesToSend),
            };
          }
          return node;
        })
      );
    },
    [setNodes]
  );

  return {
    nodes,
    isRunning,
    setNodeLinesOfCode,
    setInputValues,
    start,
    step,
    stop,
  };
}
