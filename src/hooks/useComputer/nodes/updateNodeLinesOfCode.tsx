import { doLinesOfCodeHaveError } from '../commands/helpers';
import { ComputeNode } from '../../../types';
import { MAX_LINE_OF_CODE_LENGTH, MAX_LINES_OF_CODE } from '../../../constants';

export const sanitizeLinesOfCode = (
  linesOfCode: Array<string>,
  cursorIndex: number
): [Array<string>, number] => {
  const nextLinesOfCode = [...linesOfCode];
  let nextCursorIndex = cursorIndex;

  let currentLineIndex;
  let currentLineCursorIndex = cursorIndex;
  for (
    currentLineIndex = 0;
    currentLineIndex < nextLinesOfCode.length;
    currentLineIndex += 1
  ) {
    const lineOfCodeLength = nextLinesOfCode[currentLineIndex].length;
    if (lineOfCodeLength > currentLineCursorIndex) {
      break;
    }
    currentLineCursorIndex -= 1 + lineOfCodeLength;
  }

  // If line where cursor is exceeds length then trim from cursor backwards
  // (this is to handle if they exceed length from the middle of the line)
  if (nextLinesOfCode[currentLineIndex]?.length > MAX_LINE_OF_CODE_LENGTH) {
    const extraCharCt =
      nextLinesOfCode[currentLineIndex].length - MAX_LINE_OF_CODE_LENGTH;

    nextLinesOfCode[currentLineIndex] = `${nextLinesOfCode[
      currentLineIndex
    ].substring(0, currentLineCursorIndex - extraCharCt)}${nextLinesOfCode[
      currentLineIndex
    ].substring(currentLineCursorIndex)}`;
    nextCursorIndex -= extraCharCt;
  }

  return [
    nextLinesOfCode
      .map(lineOfCode =>
        lineOfCode.toUpperCase().substring(0, MAX_LINE_OF_CODE_LENGTH)
      )
      .slice(0, MAX_LINES_OF_CODE),
    nextCursorIndex,
  ];
};

function updateNodeLinesOfCode(
  prevNode: ComputeNode,
  linesOfCode: Array<string>,
  cursorIndex = 0
): ComputeNode {
  if (prevNode.isDisabled) {
    return prevNode;
  }

  const [nextLinesOfCode, nextCursorIndex] = sanitizeLinesOfCode(
    linesOfCode,
    cursorIndex
  );
  return {
    ...prevNode,
    linesOfCode: nextLinesOfCode,
    cursorIndex: nextCursorIndex,
    hasError: doLinesOfCodeHaveError(nextLinesOfCode),
  };
}

export default updateNodeLinesOfCode;
