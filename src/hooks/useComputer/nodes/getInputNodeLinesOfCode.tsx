const getInputNodeLinesOfCode = (
  valuesToSend: Array<number>
): Array<string> => [
  ...valuesToSend.map(valueToSend => `MOV ${valueToSend} DOWN`),
  'NOP',
  'JRO -1',
];

export default getInputNodeLinesOfCode;
