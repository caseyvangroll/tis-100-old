import { useState, useCallback, useEffect } from 'react';
import { useInterval } from 'usehooks-ts';
import {
  DEFAULT_SEED,
  STEP_SPEED_MS,
  STEP_SPEED_FAST_MS,
} from '../../constants';
import {
  PuzzleType,
  TestNodeConfig,
  TestInputType,
  TestOutputType,
  GetTestValuesFn,
  InitialNodeState,
} from '../../types';
import { Tile, NodeType } from '../../constants';
import useComputer from '../useComputer';
import { getRandomIntGenerator } from '../../utils';

export default function usePuzzle(
  layout: Array<Tile>,
  inputNodes: Array<TestNodeConfig>,
  outputNodes: Array<TestNodeConfig>,
  getTestValues: GetTestValuesFn,
  initialNodeStates?: Array<InitialNodeState>
): PuzzleType {
  const computer = useComputer(
    layout,
    inputNodes,
    outputNodes,
    initialNodeStates
  );

  const [testIndex, setTestIndex] = useState(0);
  const [inputs, setInputs] = useState<Array<TestInputType>>([]);
  const [outputs, setOutputs] = useState<Array<TestOutputType>>([]);
  const [stepSpeed, setStepSpeed] = useState<number | null>(null);

  const {
    setInputValues,
    start: startComputer,
    step: stepComputer,
    stop: stopComputer,
  } = computer;
  const loadTestValues = useCallback(() => {
    if (testIndex < 3) {
      let getRandomInt = getRandomIntGenerator(DEFAULT_SEED);
      if (testIndex !== 0) {
        const seed = Math.floor(Math.random() * 100000);
        getRandomInt = getRandomIntGenerator(seed);
      }

      const testValues = getTestValues(getRandomInt);
      const nextInputs = testValues.inputs.map(input => ({
        ...input,
        curValueToSendIndex: 0,
      }));
      const nextOutputs = testValues.outputs.map(output => ({
        ...output,
        valuesReceived: [],
      }));
      setInputs(nextInputs);
      setOutputs(nextOutputs);
      setInputValues(nextInputs);
    }
  }, [testIndex, getTestValues, setInputs, setOutputs, setInputValues]);

  const onEgress = useCallback(
    (nodeType: NodeType, nodeId: string) => {
      if (nodeType === NodeType.Input) {
        setInputs(prevInputs =>
          prevInputs.map(input => {
            if (input.id === nodeId) {
              return {
                ...input,
                curValueToSendIndex: input.curValueToSendIndex + 1,
              };
            }
            return input;
          })
        );
      }
    },
    [setInputs]
  );

  const onIngress = useCallback(
    (nodeType: NodeType, nodeId: string, valueReceived: number) => {
      if (nodeType === NodeType.Output) {
        setOutputs(prevOutputs =>
          prevOutputs.map(output => {
            if (output.id === nodeId) {
              return {
                ...output,
                valuesReceived: [...output.valuesReceived, valueReceived],
              };
            }
            return output;
          })
        );
      }
    },
    [setOutputs]
  );

  const step = useCallback(() => {
    setStepSpeed(null);
    stepComputer(onEgress, onIngress);
  }, [stepComputer, onEgress, onIngress]);

  const start = useCallback(() => {
    startComputer();
    setStepSpeed(STEP_SPEED_MS);
  }, [startComputer, setStepSpeed]);

  const startFast = useCallback(() => {
    startComputer();
    setStepSpeed(STEP_SPEED_FAST_MS);
  }, [startComputer, setStepSpeed]);

  useInterval(() => stepComputer(onEgress, onIngress), stepSpeed);

  const stop = useCallback(() => {
    stopComputer();
    setStepSpeed(null);
    setTestIndex(0);
    loadTestValues();
  }, [stopComputer, setTestIndex, loadTestValues]);

  // Loads test values on first call
  useEffect(loadTestValues, [
    layout,
    inputNodes,
    outputNodes,
    getTestValues,
    loadTestValues,
  ]);

  // Check if output nodes have received all correct responses for test
  useEffect(() => {
    const isTestSuccess =
      outputs.length &&
      outputs.every(output => {
        return output.expectedValues.every(
          (expectedValue, i) =>
            i < output.valuesReceived.length &&
            output.valuesReceived[i] === expectedValue
        );
      });
    if (isTestSuccess) {
      setTestIndex(x => Math.min(x + 1, 4));
    }
  }, [setTestIndex, outputs]);

  return {
    computer,
    testIndex,
    inputs,
    outputs,
    start,
    startFast,
    step,
    stop,
  };
}
