import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { useParams, useNavigate } from 'react-router-dom';
import { Computer, TestDisplay, PuzzleStatus, Controls } from '..';
import usePuzzle from '../../hooks/usePuzzle';
import { PuzzleConfiguration, InitialNodeState } from '../../types';
import { fetchPuzzleConfiguration } from '../../queries';
import './Puzzle.css';

export interface PuzzlePureProps extends PuzzleConfiguration {
  initialNodeStates?: Array<InitialNodeState>;
}

export const PuzzlePure: React.FC<PuzzlePureProps> = ({
  name,
  description,
  creator,
  layout,
  inputNodes,
  outputNodes,
  getTestValues,
  initialNodeStates,
}) => {
  const { computer, testIndex, inputs, outputs, start, startFast, step, stop } =
    usePuzzle(
      layout,
      inputNodes,
      outputNodes,
      getTestValues,
      initialNodeStates
    );

  return (
    <div id="puzzle">
      <aside>
        <div id="sidebar-top">
          <h5>
            - "{name}" by {creator} -
          </h5>
          <PuzzleStatus
            description={description}
            testIndex={testIndex}
            isRunning={computer.isRunning}
          />
        </div>
        <div id="sidebar-bottom">
          <TestDisplay
            inputs={inputs}
            outputs={outputs}
            isRunning={computer.isRunning}
          />
          <Controls
            start={start}
            startFast={startFast}
            step={step}
            stop={stop}
          />
        </div>
      </aside>
      <main>
        <Computer {...computer} />
      </main>
    </div>
  );
};

const Puzzle: React.FC = () => {
  const { puzzleSlug = '' } = useParams();
  const navigate = useNavigate();
  const { data, isLoading, isSuccess } = useQuery(puzzleSlug, () =>
    fetchPuzzleConfiguration(puzzleSlug)
  );

  useEffect(() => {
    const escapeHandler = ({ key }: KeyboardEvent) => {
      if (key === 'Escape') {
        navigate('/');
      }
    };
    window.addEventListener('keydown', escapeHandler);
    return () => {
      window.removeEventListener('keydown', escapeHandler);
    };
  }, [navigate]);

  if (isLoading) {
    return <div id="puzzle">Loading...</div>;
  }

  if (isSuccess && data) {
    return <PuzzlePure {...data} />;
  }

  return <div id="puzzle">Error evaluating lua puzzle configuration...</div>;
};

export default Puzzle;
