import { Meta } from '@storybook/react';

import { PuzzlePure } from './Puzzle';
import { InitialNodeState } from '../../types';
import selfTestDiagnostic from '../../puzzles/self-test-diagnostic';

export default {
  title: 'Components/Puzzle',
  component: PuzzlePure,
} as Meta;

const initialNodeStates: Array<InitialNodeState> = [
  // Row 1
  { linesOfCode: ['MOV UP DOWN'] },
  {},
  { linesOfCode: ['MOV RIGHT DOWN'] },
  { linesOfCode: ['MOV UP LEFT'] },
  // Row 2
  { linesOfCode: ['MOV UP DOWN'] },
  {},
  { linesOfCode: ['MOV UP DOWN'] },
  {},
  // Row 3
  { linesOfCode: ['MOV UP DOWN'] },
  {},
  { linesOfCode: ['MOV UP RIGHT'] },
  { linesOfCode: ['MOV LEFT DOWN'] },
];

export const SelfTestDiagnostic = (): JSX.Element => (
  <PuzzlePure {...selfTestDiagnostic} initialNodeStates={initialNodeStates} />
);
