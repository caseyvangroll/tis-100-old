import React from 'react';
import { Direction } from '../../constants';
import { ReactComponent as EmptyArrowLeft } from './empty-arrow-left.svg';
import { ReactComponent as EmptyArrowRight } from './empty-arrow-right.svg';
import { ReactComponent as EmptyArrowUp } from './empty-arrow-up.svg';
import { ReactComponent as EmptyArrowDown } from './empty-arrow-down.svg';
import { ReactComponent as FilledArrowLeft } from './filled-arrow-left.svg';
import { ReactComponent as FilledArrowRight } from './filled-arrow-right.svg';
import { ReactComponent as FilledArrowUp } from './filled-arrow-up.svg';
import { ReactComponent as FilledArrowDown } from './filled-arrow-down.svg';

export interface DatabusArrowProps {
  direction: Direction;
  filled: boolean;
}

const DatabusArrow: React.FC<DatabusArrowProps> = ({ direction, filled }) => {
  if (filled) {
    switch (direction) {
      case Direction.Up:
        return <FilledArrowUp />;
      case Direction.Down:
        return <FilledArrowDown />;
      case Direction.Right:
        return <FilledArrowRight />;
      default:
        return <FilledArrowLeft />;
    }
  }
  switch (direction) {
    case Direction.Up:
      return <EmptyArrowUp />;
    case Direction.Down:
      return <EmptyArrowDown />;
    case Direction.Right:
      return <EmptyArrowRight />;
    default:
      return <EmptyArrowLeft />;
  }
};

export default React.memo(DatabusArrow);
