import React from 'react';
import { DatabusArrow } from '..';
import { Egress } from '../../types';
import { Direction } from '../../constants';
import './Input.css';

export interface InputProps {
  id?: string;
  column: number;
  egress?: Egress;
  style?: Record<string, unknown>;
}

export const Input: React.FC<InputProps> = ({
  id,
  column,
  egress,
  style = {},
}) => {
  const fullStyle = {
    ...style,
    gridRow: 1,
    gridColumn: column * 2 + 1,
  };
  return (
    <>
      <div className="input-label" style={fullStyle}>
        {id}
      </div>
      <div className="databus horizontal down" style={fullStyle}>
        <DatabusArrow
          direction={Direction.Down}
          filled={
            egress?.direction === Direction.Down && egress?.data !== undefined
          }
        />
        <div className="data">
          <p>{egress?.direction === Direction.Down && egress?.data}</p>
        </div>
      </div>
    </>
  );
};

export default React.memo(Input);
