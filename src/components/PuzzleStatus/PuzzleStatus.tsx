import React from 'react';
import './PuzzleStatus.css';

export interface PuzzleStatusProps {
  description: Array<string>;
  testIndex: number;
  isRunning: boolean;
}

export const PuzzleStatus: React.FC<PuzzleStatusProps> = ({
  description,
  testIndex,
  isRunning,
}) => {
  let lines = description;
  if (isRunning) {
    lines = [];
    for (let i = 0; i < testIndex; i += 1) {
      lines.push(`TEST ${i + 1} PASSED`);
    }
    if (testIndex <= 2) {
      lines.push(`RUNNING TEST ${testIndex + 1}...`);
    } else {
      lines.push('SUCCESS!');
    }
  }
  return (
    <div id="puzzle-status">
      {lines.map((line, i) => (
        <span key={i}>&gt; {line}</span>
      ))}
    </div>
  );
};

export default React.memo(PuzzleStatus);
