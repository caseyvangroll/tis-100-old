import React from 'react';
import { Node, Input, Output } from '..';
import { NodeType } from '../../constants';
import { ComputeNode, SetNodeLinesOfCodeFn } from '../../types';
import './Computer.css';

export interface ComputerProps {
  nodes: Array<ComputeNode>;
  isRunning: boolean;
  setNodeLinesOfCode: SetNodeLinesOfCodeFn;
}

const Computer: React.FC<ComputerProps> = ({
  nodes,
  isRunning,
  setNodeLinesOfCode,
}) => {
  return (
    <div id="computer">
      {nodes.map((node, index) => {
        if (node.type === NodeType.Input) {
          return <Input key={node.id} {...node} />;
        }

        if (node.type === NodeType.Output) {
          return <Output key={node.id} {...node} />;
        }

        return (
          <Node
            key={node.id}
            setNodeLinesOfCode={setNodeLinesOfCode}
            isRunning={isRunning}
            {...node}
          />
        );
      })}
    </div>
  );
};

export default React.memo(Computer);
