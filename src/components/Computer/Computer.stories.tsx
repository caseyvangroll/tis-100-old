import { Meta } from '@storybook/react';
import Computer from './Computer';
import useComputer from '../../hooks/useComputer';
import { Tile } from '../../constants';
import { InitialNodeState } from '../../types';

export default {
  title: 'Components/Computer',
  component: Computer,
} as Meta;

const layout = new Array(12).fill(undefined).map(() => Tile.Compute);
const onEgress = () => undefined;
const onIngress = () => undefined;
interface WrapperProps {
  initialNodeStates?: Array<InitialNodeState>;
}

export const Sandbox = ({ initialNodeStates }: WrapperProps): JSX.Element => {
  const computer = useComputer(layout, [], [], initialNodeStates);
  const step = () => computer.step(onEgress, onIngress);
  return (
    <div>
      <button onClick={step}>Step</button>
      <button onClick={computer.stop}>Stop</button>
      <Computer {...computer} />
    </div>
  );
};
