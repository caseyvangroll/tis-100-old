import React from 'react';
import { Ingress } from '../../types';
import './Output.css';

export interface OutputProps {
  id?: string;
  row: number;
  column: number;
  ingress?: Ingress;
  style?: Record<string, unknown>;
}

export const Output: React.FC<OutputProps> = ({
  id,
  row,
  column,
  style = {},
}) => {
  const fullStyle = {
    gridRow: row * 2 + 1,
    gridColumn: column * 2 + 1,
  };

  return (
    <div className="output-label" style={fullStyle}>
      {id}
    </div>
  );
};

export default React.memo(Output);
