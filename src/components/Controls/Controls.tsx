import React from 'react';
import { StartFn, StartFastFn, StepFn, StopFn } from '../../types';
import './Controls.css';

export interface ControlsProps {
  start: StartFn;
  startFast: StartFastFn;
  step: StepFn;
  stop: StopFn;
}

export const Controls: React.FC<ControlsProps> = ({
  start,
  startFast,
  step,
  stop,
}) => {
  return (
    <div id="controls">
      <div className="control">
        <button className="box-shadow" onClick={stop}>
          STOP
        </button>
      </div>
      <div className="control">
        <button className="box-shadow" onClick={step}>
          STEP
        </button>
      </div>
      <div className="control">
        <button className="box-shadow" onClick={start}>
          RUN
        </button>
      </div>
      <div className="control">
        <button className="box-shadow" onClick={startFast}>
          FAST
        </button>
      </div>
    </div>
  );
};

export default React.memo(Controls);
