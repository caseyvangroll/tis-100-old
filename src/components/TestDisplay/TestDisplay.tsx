import React from 'react';
import { TestInputType, TestOutputType } from '../../types';
import TestInputsDisplay from './TestInputsDisplay';
import TestOutputsDisplay from './TestOutputsDisplay';
import './TestDisplay.css';

export interface TestDisplayProps {
  inputs: Array<TestInputType>;
  outputs: Array<TestOutputType>;
  isRunning: boolean;
}

export const TestDisplay: React.FC<TestDisplayProps> = ({
  inputs,
  outputs,
  isRunning,
}) => {
  return (
    <div id="test-display">
      <TestInputsDisplay inputs={inputs} isRunning={isRunning} />
      <TestOutputsDisplay outputs={outputs} />
    </div>
  );
};

export default React.memo(TestDisplay);
