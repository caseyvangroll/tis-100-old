import React from 'react';
import { TestOutputType } from '../../types';

export interface TestOutputsDisplayProps {
  outputs: Array<TestOutputType>;
}

export const TestOutputsDisplay: React.FC<TestOutputsDisplayProps> = ({
  outputs,
}) => {
  return (
    <div id="test-outputs">
      {outputs.map(({ id, expectedValues, valuesReceived }) => {
        return (
          <div key={id} className="test-output-container">
            <div className="test-output-id">{id}</div>
            <div className="test-output-values">
              <div className="test-output-expected">
                {expectedValues.map((value, i) => (
                  <span key={i}>{value}</span>
                ))}
              </div>
              <div className="test-output-actual">
                {valuesReceived.map((value, i) => {
                  let style = {};
                  if (value !== expectedValues[i]) {
                    style = {
                      backgroundColor: 'red',
                    };
                  }
                  return (
                    <span key={i} style={style}>
                      {value}
                    </span>
                  );
                })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default React.memo(TestOutputsDisplay);
