import React from 'react';
import { TestInputType } from '../../types';

export interface TestInputsDisplayProps {
  inputs: Array<TestInputType>;
  isRunning: boolean;
}

export const TestInputsDisplay: React.FC<TestInputsDisplayProps> = ({
  inputs,
  isRunning,
}) => {
  return (
    <div id="test-inputs">
      {inputs.map(({ id, valuesToSend, curValueToSendIndex }) => {
        return (
          <div key={id} className="test-input-container">
            <div className="test-input-id">{id}</div>
            <div className="test-input-values">
              {valuesToSend.map((value, i) => {
                let style = {};
                if (curValueToSendIndex === i && isRunning) {
                  style = {
                    backgroundColor: 'white',
                    color: 'black',
                  };
                }
                return (
                  <span key={i} style={style}>
                    {value}
                  </span>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default React.memo(TestInputsDisplay);
