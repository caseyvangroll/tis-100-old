import React from 'react';
import { Meta } from '@storybook/react';

import Node from './Node';
import { Egress } from '../../types';
import { Direction } from '../../constants';

export default {
  title: 'Components/Node',
  component: Node,
} as Meta;

interface PartialNodeProps {
  isDisabled?: boolean;
  isRunning?: boolean;
  curLineIndex?: number;
  linesOfCode?: Array<string>;
  acc?: number;
  bak?: number;
  hasError?: boolean;
  egress?: Egress;
}

const NodeWrapper: React.FC<PartialNodeProps> = initialProps => {
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: 'repeat(3, min-content)',
        gridTemplateRows: 'repeat(3, min-content)',
        gap: '0.5rem',
      }}
    >
      <Node
        acc={5}
        bak={7}
        row={1}
        column={1}
        curLineIndex={0}
        cursorIndex={0}
        linesOfCode={[]}
        hasError={false}
        isDisabled={false}
        isRunning={true}
        setNodeLinesOfCode={() => undefined}
        style={{
          width: '200px',
          height: '200px',
        }}
        {...initialProps}
      />
    </div>
  );
};

export const Empty = (): JSX.Element => <NodeWrapper acc={0} bak={0} />;

export const Disabled = (): JSX.Element => (
  <NodeWrapper isDisabled={true} isRunning={false} />
);

export const Invalid = (): JSX.Element => (
  <NodeWrapper hasError={true} isRunning={false} linesOfCode={['NOT VALID']} />
);

export const Valid = (): JSX.Element => (
  <NodeWrapper
    linesOfCode={['MOV 1, UP']}
    egress={{ direction: Direction.Up, data: 1 }}
  />
);
