import React from 'react';
import classNames from 'classnames';
import LinesOfCode from './LinesOfCode';
import { DatabusArrow } from '..';
import { Egress, SetNodeLinesOfCodeFn } from '../../types';
import { Direction } from '../../constants';
import './Node.css';

export interface NodeProps {
  row: number;
  column: number;
  cursorIndex: number;
  linesOfCode: Array<string>;
  curLineIndex: number;
  isDisabled: boolean;
  hasError: boolean;
  acc: number;
  bak: number;
  isRunning: boolean;
  egress?: Egress;
  hasUp?: boolean;
  hasRight?: boolean;
  hasDown?: boolean;
  hasLeft?: boolean;
  style?: Record<string, unknown>;
  setNodeLinesOfCode: SetNodeLinesOfCodeFn;
}

export const Node: React.FC<NodeProps> = ({
  row,
  column,
  cursorIndex,
  linesOfCode,
  curLineIndex,
  isDisabled,
  hasError,
  acc,
  bak,
  isRunning,
  egress,
  hasUp,
  hasRight,
  hasDown,
  hasLeft,
  style = {},
  setNodeLinesOfCode,
}) => {
  const gridRow = row * 2 + 2; // Top row of grid is always for input egress
  const gridColumn = column * 2 + 1;

  const arrowUp = !isDisabled && hasUp && (
    <div
      className="databus horizontal up"
      style={{ gridRow: gridRow - 1, gridColumn }}
    >
      <div className="data">
        <p>{egress?.direction === Direction.Up && egress?.data}</p>
      </div>
      <DatabusArrow
        direction={Direction.Up}
        filled={
          egress?.direction === Direction.Up && egress?.data !== undefined
        }
      />
    </div>
  );
  const arrowRight = !isDisabled && hasRight && (
    <div
      className="databus vertical right"
      style={{ gridRow: gridRow, gridColumn: gridColumn + 1 }}
    >
      <div className="data">
        <p>{egress?.direction === Direction.Right && egress?.data}</p>
      </div>
      <DatabusArrow
        direction={Direction.Right}
        filled={
          egress?.direction === Direction.Right && egress?.data !== undefined
        }
      />
    </div>
  );
  const arrowDown = !isDisabled && hasDown && (
    <div
      className="databus horizontal down"
      style={{ gridRow: gridRow + 1, gridColumn }}
    >
      <DatabusArrow
        direction={Direction.Down}
        filled={
          egress?.direction === Direction.Down && egress?.data !== undefined
        }
      />
      <div className="data">
        <p>{egress?.direction === Direction.Down && egress?.data}</p>
      </div>
    </div>
  );
  const arrowLeft = !isDisabled && hasLeft && (
    <div
      className="databus vertical left"
      style={{ gridRow, gridColumn: gridColumn - 1 }}
    >
      <DatabusArrow
        direction={Direction.Left}
        filled={
          egress?.direction === Direction.Left && egress?.data !== undefined
        }
      />
      <div className="data">
        <p>{egress?.direction === Direction.Left && egress?.data}</p>
      </div>
    </div>
  );

  return (
    <>
      <div
        className={classNames({
          node: true,
          isDisabled,
        })}
        style={{
          ...style,
          gridRow,
          gridColumn,
        }}
      >
        <LinesOfCode
          row={row}
          column={column}
          cursorIndex={cursorIndex}
          linesOfCode={linesOfCode}
          curLineIndex={curLineIndex}
          isDisabled={isDisabled}
          hasError={hasError}
          isRunning={isRunning}
          setNodeLinesOfCode={setNodeLinesOfCode}
        />
        <div className="acc data-container double-border-inner">
          <div className="data">
            <p className="label">{!isDisabled && 'ACC'}</p>
            <p>{!isDisabled && String(acc)}</p>
          </div>
        </div>

        <div className="bak data-container double-border-inner">
          <div className="data">
            <p className="label">{!isDisabled && 'BAK'}</p>
            <p>{!isDisabled && `(${bak})`}</p>
          </div>
        </div>

        <div className="last data-container double-border-inner">
          <div className="data">
            <p className="label">{!isDisabled && 'LAST'}</p>
            <p>{!isDisabled && 'todo'}</p>
          </div>
        </div>
        <div className="mode data-container double-border-inner">
          <div className="data">
            <p className="label">{!isDisabled && 'MODE'}</p>
            <p>{!isDisabled && 'todo'}</p>
          </div>
        </div>
      </div>
      {arrowUp}
      {arrowRight}
      {arrowDown}
      {arrowLeft}
    </>
  );
};

export default React.memo(Node);
