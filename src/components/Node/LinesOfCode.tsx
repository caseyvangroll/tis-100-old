import classNames from 'classnames';
import React, { useRef } from 'react';
import { SetNodeLinesOfCodeFn } from '../../types';
import './LinesOfCode.css';

// document.execCommand("defaultParagraphSeparator", false, "span");
export interface LinesOfCodeProps {
  row: number;
  column: number;
  cursorIndex: number;
  linesOfCode: Array<string>;
  curLineIndex: number;
  isRunning: boolean;
  isDisabled: boolean;
  hasError: boolean;
  setNodeLinesOfCode: SetNodeLinesOfCodeFn;
}

export const LinesOfCode: React.FC<LinesOfCodeProps> = ({
  row,
  column,
  cursorIndex,
  linesOfCode,
  curLineIndex,
  isRunning,
  isDisabled,
  hasError,
  setNodeLinesOfCode,
}) => {
  const inputRef = useRef<HTMLTextAreaElement>(null);
  const handleChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const nextCursorIndex = event.target.selectionStart || 0;
    const nextTextAreaValue = event.target.value;
    setNodeLinesOfCode(
      row,
      column,
      nextTextAreaValue.split('\n'),
      nextCursorIndex
    );
  };

  React.useLayoutEffect(() => {
    if (inputRef.current) {
      inputRef.current.selectionStart = cursorIndex;
      inputRef.current.selectionEnd = cursorIndex;
    }
  });

  if (isDisabled) {
    return (
      <div className="code-container disabled-mode">
        <pre className="code-output">
          <code>
            {/* <span>
              &#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;&#9600;
            </span> */}
            <span>COMMUNICATION</span>
            <span>FAILURE</span>
            {/* <span>
              &#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;&#9604;
            </span> */}
          </code>
        </pre>
      </div>
    );
  }

  if (isRunning) {
    return (
      <div className="code-container run-mode">
        <pre className="code-output">
          <code>
            {linesOfCode.map((lineOfCode, i) => (
              <span
                key={i}
                className={classNames({
                  isCurrentLine: curLineIndex === i,
                })}
              >
                {lineOfCode}
              </span>
            ))}
          </code>
        </pre>
      </div>
    );
  }

  return (
    <div
      className={classNames('code-container', 'edit-mode', {
        hasError,
        isDisabled,
      })}
    >
      <textarea
        className="code-input"
        value={linesOfCode.join('\n')}
        onChange={handleChange}
        ref={inputRef}
      />
    </div>
  );
};

export default React.memo(LinesOfCode);
