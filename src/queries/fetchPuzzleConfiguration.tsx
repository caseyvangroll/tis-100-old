import { PuzzleConfiguration } from '../types';
import puzzles from '../puzzles';

const fetchPuzzleConfiguration = (
  puzzleSlug: string
): PuzzleConfiguration | undefined => {
  // For now we're not fetching puzzle config by remotely evaluating puzzle lua source code.
  // We're just going to grab from the json configs included in the bundle.
  return puzzles.find(
    (puzzle: PuzzleConfiguration) => puzzleSlug === puzzle.slug
  );
};

export default fetchPuzzleConfiguration;
