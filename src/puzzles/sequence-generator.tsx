import { Tile } from '../constants';
import { RandomIntGenerator, TestValues } from '../types';

const IN_A = 'IN.A';
const IN_B = 'IN.B';
const OUT = 'OUT';

const puzzle = {
  slug: 'sequence-generator',
  name: 'SEQUENCE GENERATOR',
  description: [
    `SEQUENCES ARE ZERO-TERMINATED`,
    `READ VALUES FROM ${IN_A} AND ${IN_B}`,
    `WRITE THE LESSER VALUE TO ${OUT}`,
    `WRITE THE GREATER VALUE TO ${OUT}`,
    `WRITE0 TO END THE SEQUENCE`,
  ],
  creator: 'ZACHTRONICS',
  layout: [
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Compute,
  ],
  inputNodes: [
    {
      id: IN_A,
      column: 1,
    },
    {
      id: IN_B,
      column: 2,
    },
  ],
  outputNodes: [
    {
      id: OUT,
      column: 2,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const aInputValues = new Array(13).fill(0).map(() => getRandomInt(10, 99));
    const bInputValues = new Array(13).fill(0).map(() => getRandomInt(10, 99));
    const outputValues: Array<number> = [];
    for (let i = 0; i < aInputValues.length; i += 1) {
      const a = aInputValues[i];
      const b = bInputValues[i];
      if (a < b) {
        outputValues.push(a);
        outputValues.push(b);
      } else {
        outputValues.push(b);
        outputValues.push(a);
      }
      outputValues.push(0);
    }
    return {
      inputs: [
        {
          id: IN_A,
          valuesToSend: aInputValues,
        },
        {
          id: IN_B,
          valuesToSend: bInputValues,
        },
      ],
      outputs: [
        {
          id: OUT,
          expectedValues: outputValues,
        },
      ],
    };
  },
};

export default puzzle;
