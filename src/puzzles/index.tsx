import { default as selfTestDiagnostic } from './self-test-diagnostic';
import { default as differentialConverter } from './differential-converter';
import { default as signalAmplifier } from './signal-amplifier';
import { default as signalComparator } from './signal-comparator';
import { default as signalMultiplexer } from './signal-multiplexer';
import { default as sequenceGenerator } from './sequence-generator';
import { default as sequenceCounter } from './sequence-counter';

const puzzles = [
  selfTestDiagnostic,
  differentialConverter,
  signalAmplifier,
  signalComparator,
  signalMultiplexer,
  sequenceGenerator,
  sequenceCounter,
];

export default puzzles;
