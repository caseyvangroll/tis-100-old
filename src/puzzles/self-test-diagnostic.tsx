import { Tile } from '../constants';
import { RandomIntGenerator, TestValues } from '../types';

const IN_X = 'IN.X';
const IN_A = 'IN.A';
const OUT_X = 'OUT.X';
const OUT_A = 'OUT.A';

const selfTestDiagnostic = {
  slug: 'self-test-diagnostic',
  name: 'SELF-TEST DIAGNOSTIC',
  description: [
    `READ A VALUE FROM ${IN_X} AND WRITE THE VALUE TO ${OUT_X}`,
    `READ A VALUE FROM ${IN_A} AND WRITE THE VALUE TO ${OUT_A}`,
  ],
  creator: 'ZACHTRONICS',
  layout: [
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Compute,
  ],
  inputNodes: [
    {
      id: IN_X,
      column: 0,
    },
    {
      id: IN_A,
      column: 3,
    },
  ],
  outputNodes: [
    {
      id: OUT_X,
      column: 0,
    },
    {
      id: OUT_A,
      column: 3,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const xInputValues = new Array(39).fill(0).map(() => getRandomInt(10, 99));
    const aInputValues = new Array(39).fill(0).map(() => getRandomInt(10, 99));
    return {
      inputs: [
        {
          id: IN_X,
          valuesToSend: xInputValues,
        },
        {
          id: IN_A,
          valuesToSend: aInputValues,
        },
      ],
      outputs: [
        {
          id: OUT_X,
          expectedValues: [...xInputValues],
        },
        {
          id: OUT_A,
          expectedValues: [...aInputValues],
        },
      ],
    };
  },
};

export default selfTestDiagnostic;
