import { Tile } from '../constants';
import { RandomIntGenerator, TestValues } from '../types';

const IN = 'IN';
const OUT_S = 'OUT.S';
const OUT_L = 'OUT.L';

const puzzle = {
  slug: 'sequence-counter',
  name: 'SEQUENCE COUNTER',
  description: [
    'SEQUENCES ARE ZERO-TERMINATED',
    `READ A SEQUENCE FROM ${IN}`,
    `WRITE THE SUM TO ${OUT_S}`,
    `WRITE THE LENGTH TO ${OUT_L}`,
  ],
  creator: 'ZACHTRONICS',
  layout: [
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
  ],
  inputNodes: [
    {
      id: IN,
      column: 1,
    },
  ],
  outputNodes: [
    {
      id: OUT_S,
      column: 1,
    },
    {
      id: OUT_L,
      column: 2,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const inputValues = [];
    const sOutputValues = [];
    const lOutputValues = [];
    while (inputValues.length < 34) {
      const sequenceLength = getRandomInt(0, 5);
      lOutputValues.push(sequenceLength);
      const sequence = new Array(sequenceLength)
        .fill(0)
        .map(() => getRandomInt(10, 99));
      sOutputValues.push(sequence.reduce((acc, n) => acc + n, 0));
      inputValues.push(...sequence, 0);
    }
    if (inputValues.length < 39) {
      const sequenceLength = 38 - inputValues.length;
      lOutputValues.push(sequenceLength);
      const sequence = new Array(sequenceLength)
        .fill(0)
        .map(() => getRandomInt(10, 99));
      sOutputValues.push(sequence.reduce((acc, n) => acc + n, 0));
      inputValues.push(...sequence, 0);
    }

    return {
      inputs: [
        {
          id: IN,
          valuesToSend: inputValues,
        },
      ],
      outputs: [
        {
          id: OUT_S,
          expectedValues: sOutputValues,
        },
        {
          id: OUT_L,
          expectedValues: lOutputValues,
        },
      ],
    };
  },
};

export default puzzle;
