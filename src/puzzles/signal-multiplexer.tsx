import { Tile } from '../constants';
import { RandomIntGenerator, TestValues } from '../types';

const IN_A = 'IN.A';
const IN_S = 'IN.S';
const IN_B = 'IN.B';
const OUT = 'OUT';

const puzzle = {
  slug: 'signal-multiplexer',
  name: 'SIGNAL MULTIPLEXER',
  description: [
    `READ VALUES FROM ${IN_A} AND ${IN_B}`,
    `READ A VALUE FROM ${IN_S}`,
    `WRITE ${IN_A} WHEN ${IN_S} = -1`,
    `WRITE ${IN_B} WHEN ${IN_S} = 1`,
    `WRITE ${IN_A} + ${IN_B} WHEN ${IN_S} = 0`,
  ],
  creator: 'ZACHTRONICS',
  layout: [
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
    Tile.Damaged,
    Tile.Compute,
    Tile.Compute,
    Tile.Compute,
  ],
  inputNodes: [
    {
      id: IN_A,
      column: 1,
    },
    {
      id: IN_S,
      column: 2,
    },
    {
      id: IN_B,
      column: 3,
    },
  ],
  outputNodes: [
    {
      id: OUT,
      column: 2,
    },
  ],
  getTestValues: (getRandomInt: RandomIntGenerator): TestValues => {
    const aInputValues = new Array(39).fill(0).map(() => getRandomInt(-30, 0));
    const sInputValues = new Array(39).fill(0).map(() => getRandomInt(-1, 1));
    const bInputValues = new Array(39).fill(0).map(() => getRandomInt(0, 30));
    const outputValues = sInputValues.map((s, i) => {
      switch (s) {
        case -1:
          return aInputValues[i];
        case 1:
          return bInputValues[i];
        default:
          return aInputValues[i] + bInputValues[i];
      }
    });
    return {
      inputs: [
        {
          id: IN_A,
          valuesToSend: aInputValues,
        },
        {
          id: IN_S,
          valuesToSend: sInputValues,
        },
        {
          id: IN_B,
          valuesToSend: bInputValues,
        },
      ],
      outputs: [
        {
          id: OUT,
          expectedValues: outputValues,
        },
      ],
    };
  },
};

export default puzzle;
