export enum Direction {
  Up = 'UP',
  Right = 'RIGHT',
  Down = 'DOWN',
  Left = 'LEFT',
}

export enum Orientation {
  Vertical = 'Vertical',
  Horizontal = 'Horizontal',
}

export enum Mode {
  Idle = 'IDLE',
  Read = 'READ',
  Run = 'RUN',
  Write = 'WRTE',
}

export const LAYOUT_WIDTH = 4;
export const LAYOUT_HEIGHT = 3;
export const DEFAULT_SEED = 123456789;
export const MAX_LINE_OF_CODE_LENGTH = 18;
export const MAX_LINES_OF_CODE = 13;
export const STEP_SPEED_MS = 400;
export const STEP_SPEED_FAST_MS = 50;

export enum NodeType {
  Input = 'Input',
  Output = 'Output',
  Compute = 'Compute',
}

export enum Tile {
  Compute = 'Compute',
  Memory = 'Memory',
  Damaged = 'Damaged',
}
