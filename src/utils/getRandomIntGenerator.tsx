import faker from 'faker';
import { RandomIntGenerator } from '../types';

function getRandomIntGenerator(seed: number): RandomIntGenerator {
  faker.seed(seed);

  return (inclusiveMin: number, inclusiveMax: number) => {
    return faker.datatype.number(inclusiveMax - inclusiveMin) + inclusiveMin;
  };
}

export default getRandomIntGenerator;
