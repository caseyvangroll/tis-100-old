import React from 'react'
import { QueryClientProvider, QueryClient } from "react-query";
import '../src/index.css';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

// Set up client for react-query
const queryClient = new QueryClient();
export const decorators=[
  (Story) => (
    <QueryClientProvider client={queryClient}>
      <Story />
    </QueryClientProvider>
  ),
]
