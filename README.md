## Features To-Do
- Hide databuses that don't have any commands that may use them in program
- Prettier loading screen when fetching puzzle config
- Prettier error screen when failed to fetch/generate puzzle config
- Come up with actual app experience
- Stories for all components
- More unit tests in critical areas
- Implement node MODE and IDLE%
- Cypress tests
- Deployment configuration to domain


### start
- set interval to call step function
- stop calling "step" if all tests are finished and report test results

### step
- step one time
- if reach end of test set...
  - another test set exists...
    - reset all state of computer
    - initialize computer with new test set
  - no more test set exists...
    - reset state of all nodes
    - leave current test set results

### stop
- reset to first test set
- reset state of all nodes